﻿using System.Collections.Generic;

namespace ProSkive.Clients.Archive.V1.Resources
{
    public class ArchiveResourceApiResource
    {
        public string Id { get; set; }
        public string Hash { get; set; }
        public List<ArchiveInstanceApiResource> Instances { get; set; }
    }
}