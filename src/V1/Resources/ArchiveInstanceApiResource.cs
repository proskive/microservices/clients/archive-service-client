﻿using System;
using System.Collections.Generic;

namespace ProSkive.Clients.Archive.V1.Resources
{
    public class ArchiveInstanceApiResource
    {
        public int Version { get; set; }
        public object Data { get; set; }
        public DateTime Created { get; set; }
        public List<string> Diff { get; set; }
    }
}