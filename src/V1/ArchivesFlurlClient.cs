﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using ProSkive.Clients.Archive.V1.Resources;

namespace ProSkive.Clients.Archive.V1
{
    public class ArchivesFlurlClient : IArchiveClient
    {
        public string BaseAddress { get; }
        
        public ArchivesFlurlClient(string baseAddress)
        {
            BaseAddress = baseAddress;
        }
        
        
        public async Task<HttpResponseMessage> GetAllArchivesWithResponseAsync(string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment("api/v1/archive");

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }

        public async Task<List<ArchiveResourceApiResource>> GetAllArchivesAsync(string token = null)
        {
            return await GetAllArchivesWithResponseAsync(token)
                .ReceiveJson<List<ArchiveResourceApiResource>>();
        }

        public async Task<HttpResponseMessage> GetArchiveWithResponseAsync(string hash, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/archive/{hash}");

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }

        public async Task<ArchiveResourceApiResource> GetArchiveAsync(string hash, string token = null)
        {
            return await GetArchiveWithResponseAsync(hash, token)
                .ReceiveJson<ArchiveResourceApiResource>();
        }

        public async Task<HttpResponseMessage> GetCountWithResponseAsync(string hash, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/archive/{hash}/count");

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }

        public async Task<int> GetCountArchiveAsync(string hash, string token = null)
        {
            return await GetCountWithResponseAsync(hash, token)
                .ReceiveJson<int>();
        }

        public async Task<HttpResponseMessage> GetSpecificVersionWithResponseAsync(string hash, int version, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/archive/{hash}/{version}");

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }

        public async Task<ArchiveInstanceApiResource> GetSpecificVersionArchiveAsync(string hash, int version, string token = null)
        {
            return await GetSpecificVersionWithResponseAsync(hash, version, token)
                .ReceiveJson<ArchiveInstanceApiResource>();
        }

        public async Task<HttpResponseMessage> SaveArchiveWithResponseAsync(string hash, object data, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/archive/{hash}");

            return await CreateRequestWithToken(url, token)
                .PostJsonAsync(data);
        }

        public async Task<ArchiveInstanceApiResource> SaveArchiveAsync(string hash, object data, string token = null)
        {
            return await SaveArchiveWithResponseAsync(hash, data, token)
                .ReceiveJson<ArchiveInstanceApiResource>();
        }

        private IFlurlRequest CreateRequestWithToken(Url url, string token)
        {
            return string.IsNullOrWhiteSpace(token) ? new FlurlRequest(url) : url.WithOAuthBearerToken(token);
        }
    }
}