﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProSkive.Clients.Archive.V1.Resources;

namespace ProSkive.Clients.Archive.V1
{
    public interface IArchiveClient
    {
        Task<HttpResponseMessage> GetAllArchivesWithResponseAsync(string token = null);
        Task<List<ArchiveResourceApiResource>> GetAllArchivesAsync(string token = null);
        Task<HttpResponseMessage> GetArchiveWithResponseAsync(string hash, string token = null);
        Task<ArchiveResourceApiResource> GetArchiveAsync(string hash, string token = null);
        Task<HttpResponseMessage> GetCountWithResponseAsync(string hash, string token = null);
        Task<int> GetCountArchiveAsync(string hash, string token = null);
        Task<HttpResponseMessage> GetSpecificVersionWithResponseAsync(string hash, int version, string token = null);
        Task<ArchiveInstanceApiResource> GetSpecificVersionArchiveAsync(string hash, int version, string token = null);
        Task<HttpResponseMessage> SaveArchiveWithResponseAsync(string hash, object data, string token = null);
        Task<ArchiveInstanceApiResource> SaveArchiveAsync(string hash, object data,  string token = null);
    }
}