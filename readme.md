# ProSkive.Clients.Archive

ProSkive client library for using the archive service in C#.

## Features

Contains common API models and better responses for:
  - Common Resources
     - Health Resource
     - Info Resource
  - Error Resources
     - Bad Request Resource
     - Internal Server Error Resource
     - Resource not Found Resource

## License (GPLv3)

```
ProSkive
Copyright (C) 2018  Medical Informatics Group (MIG) Frankfurt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```